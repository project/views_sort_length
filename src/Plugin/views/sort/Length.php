<?php

namespace Drupal\views_sort_length\Plugin\views\sort;

use Drupal\views\Plugin\views\sort\SortPluginBase;

/**
 * Sort handler for ordering by length of field.
 *
 * @ingroup views_sort_handlers
 *
 * @ViewsSort("views_sort_length")
 */
class Length extends SortPluginBase {

  /**
   * {@inheritDoc}
   */
  public function query() {
    $this->ensureMyTable();

    $this->query->addOrderBy(NULL, 'LENGTH(' . $this->tableAlias . '.' . $this->realField . ')', $this->options['order'], $this->field);
  }

}
